module gitea.com/tango/tpongo2

go 1.20

require (
	gitea.com/lunny/tango v0.6.6
	github.com/flosch/pongo2/v6 v6.0.0
)

require gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e // indirect

tpongo2 [![Build Status](https://drone.gitea.com/api/badges/tango/tpongo2/status.svg)](https://drone.gitea.com/tango/tpongo2) [![](http://gocover.io/_badge/gitea.com/tango/tpongo2)](http://gocover.io/gitea.com/tango/tpongo2)
======

Middleware tpongo2 is a [pongo2](https://github.com/flosch/pongo2).**v3** template engine support for [Tango](https://gitea.com/lunny/tango). 

## Installation

    go get gitea.com/tango/tpongo2

## Simple Example

```Go
package main

import (
    "gitea.com/lunny/tango"
    "gitea.com/tango/tpongo2"
    "github.com/flosch/pongo2"
)

type RenderAction struct {
    tpongo2.Renderer
}

func (a *RenderAction) Get() error {
    return a.RenderString("Hello {{ name }}!", pongo2.Context{
        "name": "tango",
    })
}

func main() {
    o := tango.Classic()
    o.Use(tpongo2.New())
    o.Get("/", new(RenderAction))
}
```

## Getting Help

- [API Reference](https://godoc.org/gitea.com/tango/tpongo2)
